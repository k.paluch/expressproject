var express = require('express');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Courses registration form with Express' });
});

//moved to coursesRegistration.js:
//const myStorage = require('../storage/coursesStorage')
// router.get("/", (req, res) => {
//   res.render('courseForm', {})
// })
//
//   router.post("/saveCourse", (req,res) => {
//     const {name, code, price, length} = req.body;
//     const course = {name: name, code: code, price: price, length:length}
//
//     myStorage.addCourse(course)
//
//
//     res.render("coursesView", {
//       title: "Your course has been created",
//       name
//     })
//
//   })



module.exports = router;
