var express = require('express');
var router = express.Router();



router.get("/", (req, res) => {
    res.render('coursesList', {title: 'Courses List'});
});

// const myStorage = require('../storage/coursesStorage')
//
// router.get("/", (req, res) => {
//     console.log(myStorage.getAllCourses())
//
//     res. render("saved", {})
// })
//
// router.post("saved", (req, res) => {
//     const {name, code, price, length } = req.body;
//
//     res.render('coursesList', {
//         title: "courses list",
//         name,
//         code,
//         price,
//         length
//     })
// })

// var sys  = require('sys'),
//     http = require('http');
//
// var number = 0;
//
// http.createServer(function (req, res) {
//     console.log(req.method, req.url);
//
//     res.writeHead(200, {'Content-Type': 'text/html'});
//     res.write('<h1>Number is: ' + number + '</h1>');
//     res.end();
//
//     number++;
//
// }).listen(8000);
//
// sys.puts('Server running at http://127.0.0.1:8000/');

module.exports = router
