var express = require('express');
var router = express.Router();

const myStorage = require('../storage/coursesStorage')


router.get("/", (req, res) => {
    res.render('courseForm', {title: 'course registration'});
});


router.post('/saveCourse', (req,res) => {
    const {name, code, price, length} = req.body;
    const course = {name: name, code: code, price: price, length:length}

    myStorage.addCourse(course)

//odsyłam do widoku saved.hbs -->
    res.render('saved', {
        title: "Your course has been created",
        course
    })
})



module.exports = router;

